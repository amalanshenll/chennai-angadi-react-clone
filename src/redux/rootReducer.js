import { combineReducers } from 'redux'
import fetchUserListReducer from '../header/redux/reducer'
import fetchOTPListReducer from '../header/OTPredux/reducer'
import loginListReducer from '../header/loginredux/reducer'
import fetcCategorigeListReducer from '../categoriepage/redux/reducer'
import ProductsListReducer from '../categoriepage/productredux/reducer'
import WishListReducer from '../mywishlist/redux/reducer'
import MyWishListReducer from '../mywishlist/mywishlistredux/reducer'
import ProductDetailListReducer from '../productsdetail/redux/reducer'
import AddToCartListReducer from '../categoriepage/addtocartredux/reducer'
import GetAddToCartListReducer from '../header/getaddtocartredux/reducer'
import fetcProfileReducer from '../myprofile/redux/reducer'
import AddressProfileReducer from '../Addressprofile/redux/reducer'
import dropzoneReducer from '../fileUpload/redux/reducer'
import UserProfileReducer from '../Userprofile/redux/reducer'
import DeleteProfileList from '../Userprofile/deleteredux /reducer'
import DeleteAddtocartReducer from '../checkoutpage/deleteredux /reducer'
import CheckoutReducer from '../checkoutpage/checkoutredux/reducer'
import GetCheckoutReducer from '../checkoutpage/checkoutgetredux /reducer'

const rootReducer = combineReducers({
  fetchUserListReducer,
  fetchOTPListReducer,
  loginListReducer,
  fetcCategorigeListReducer,
  ProductsListReducer,
  ProductDetailListReducer,
  fetcProfileReducer,
  WishListReducer,
  MyWishListReducer,
  ProductDetailListReducer,
  AddToCartListReducer,
  GetAddToCartListReducer,
  AddressProfileReducer,
  dropzoneReducer,
  UserProfileReducer,
  DeleteProfileList,
  DeleteAddtocartReducer,
  CheckoutReducer,
  GetCheckoutReducer
})

export default rootReducer
