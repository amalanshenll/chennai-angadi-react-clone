import { all } from 'redux-saga/effects'
import fetchListSaga from '../header/redux/saga'
import fetchOTPSaga from '../header/OTPredux/saga'
import loginListSaga from '../header/loginredux/saga'
import fetchCategorieSaga from '../categoriepage/redux/saga'
import fetchProductsSaga from '../categoriepage/productredux/saga'
import fetchWishlistSaga from '../mywishlist/redux/saga'
import fetchMyWishListSaga from '../mywishlist/mywishlistredux/saga'
import fetchProductDetailSaga from '../productsdetail/redux/saga'
import fetchAddToCartSaga from '../categoriepage/addtocartredux/saga'
import fetchGetAddToCartSaga from '../header/getaddtocartredux/saga'
import fetchProfileSaga from '../myprofile/redux/saga'
import fetchAddressProfileSaga from '../Addressprofile/redux/saga'
import uploadImageSaga from '../fileUpload/redux/saga'
import fetchUserProfileSaga from '../Userprofile/redux/saga'
import fetchDeleteProfileSaga from '../Userprofile/deleteredux /saga'
import fetchDeleteAddtocartSaga from '../checkoutpage/deleteredux /saga'
import fetchCheckoutSaga from '../checkoutpage/checkoutredux/saga'
import fetchGetCheckoutSaga from '../checkoutpage/checkoutgetredux /saga'

export default function * rootSaga () {
  yield all([
    ...fetchListSaga,
    ...fetchOTPSaga,
    ...loginListSaga,
    ...fetchCategorieSaga,
    ...fetchProductsSaga,
    ...fetchProductDetailSaga,
    ...fetchProfileSaga,
    ...fetchWishlistSaga,
    ...fetchMyWishListSaga,
    ...fetchProductDetailSaga,
    ...fetchAddToCartSaga,
    ...fetchGetAddToCartSaga,
    ...fetchAddressProfileSaga,
    ...uploadImageSaga,
    ...fetchUserProfileSaga,
    ...fetchDeleteProfileSaga,
    ...fetchDeleteAddtocartSaga,
    ...fetchCheckoutSaga,
    ...fetchGetCheckoutSaga,
  ])
}
