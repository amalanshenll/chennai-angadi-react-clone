import React, { useState, useEffect} from 'react'
import '../landinpage/index.css'
import Termsheaders from '../header/CommonHeader'
import termunderline_img from '../assets/images/termunderline_img.png'
import Footer from '../components/Footer';
import { MdAddLocation } from "react-icons/md"
import { FiEdit } from "react-icons/fi"
import { RiDeleteBin5Line } from "react-icons/ri"
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai"
import checkout_img from '../assets/images/checkout_img.png'
import totalline_img from '../assets/images/totalline_img.png'
import { connect } from 'react-redux'
import { GetGetCheckout } from '../checkoutpage/checkoutgetredux /action'
import {
  STATUS_RESPONSE
} from '../helpers/const'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardText,
  Alert
} from 'reactstrap';

function CheckoutPage(props) {

  const {
    GetCheckouts,
    dispatch,
    statusCHT,
    match
  } = props

  const matchId = match && match.params && match.params.id

  useEffect(() => {
    if (matchId) {
      dispatch(GetGetCheckout(matchId))
    } 
  }, [])

  return (
    <Container className="themed-container" fluid={true}>
      <div>
        <Termsheaders />
      </div>
      <div className="terms-heading">
        <h1 className="terms-head">Checkout</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <Col xs={12} md={12} mx-auto className="mb-5">
        <div>
          <Card className="checkout-card">
            <CardTitle className="checkout-second-head">
              Delivery Address
            </CardTitle>
            <CardBody>
              <div className="checkout-address mb-2">
                <span className="checkout-location" ><MdAddLocation /></span>
                <span className="checkout-add-text">Add New Address</span>
              </div>
              <div className="checkout-address1 mb-2">
                <span className="checkout-second-head1 ">
                  William Jacob
                  </span>
                <span className="checkout-fiedit"><FiEdit /></span>
                <span className="checkout-contact">9876543210</span>
                <span className="checkout-delete-btn"><RiDeleteBin5Line /></span>
                <p className="checkout-street">
                  Plot No.1069, Munusamy Salai,
                      </p>
                <span className="checkout-street1">
                  K.K Nagar, Chennai – 600078.
                      </span>
                <p className="checkout-delivery" >
                  Delivery Address
                      </p>
                <span className="checkout-heart">
                  <AiOutlineHeart />
                </span>
              </div>
              <div className="checkout-address1 mb-4">
                <span className="checkout-second-head1 ">
                  William Jacob
                  </span>
                <span className="checkout-fiedit"><FiEdit /></span>
                <span className="checkout-contact">9876543210</span>
                <span className="checkout-delete-btn"><RiDeleteBin5Line /></span>
                <p className="checkout-street">
                  Plot No.1069, Munusamy Salai,
                </p>
                <span className="checkout-street1">
                  K.K Nagar, Chennai – 600078.
                </span>
                <p className="checkout-delivery1" >
                  Delivery Address
                  </p>
                <span className="checkout-heart1">
                  <AiFillHeart />
                </span>
              </div>
              <Row>
                <Col xs={12} md={12} mx-auto className="mb-3">
                  <h1 className="checkout-order">Order Summary</h1>
                </Col>
                <Col xs={12} md={2} mx-auto className="mb-5">
                  <CardImg className="checkout-img" src={checkout_img} alt="Card image cap" />
                </Col>
                <Col xs={12} md={4} mx-auto className="mb-5">
                  <div>
                    <p className="checkout-order-no mb-0">#20323652</p>
                    <p className="checkout-order-organic mb-0">Organic Strawberry</p>
                    <p className="checkout-order-kg mb-0">0.5kg</p>
                    <span class="categires-webrupee">&#x20B9;</span>
                    <span className='amount' >200</span>
                  </div>
                </Col>
                <Col xs={12} md={4} mx-auto className="mb-5">
                  <h1 className="checkout-order-date mb-0">Order Date</h1>
                  <p className="checkout-order-date1">02-04-2021</p>
                </Col>
                <Col xs={12} md={2} mx-auto className="mb-5">
                  <h1 className="checkout-order-deliver mb-0">To Deliver On</h1>
                  <p className="checkout-order-date2">22-04-2021</p>
                </Col>
              </Row>
              <Row className="mb-4">
                <Col xs={12} md={12} mx-auto className="mb-2">
                  <h1 className="checkout-order">Bill Details</h1>
                </Col>
                <Col xs={12} md={12} mx-auto>
                  <div className="checkout-bill">
                    <Row>
                      <Col xs={12} md={11} mx-auto>
                        <p className="checkout-total">Subtotal</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto>
                        <div className="checkout-totals">
                          <span className="checkout-rupee">&#x20B9;</span>
                          <span className='checkout-rupee' >200</span>
                        </div>
                      </Col>
                      <Col xs={12} md={11} mx-auto>
                        <p className="checkout-total1">Coupon Applied</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto className="mb-0">
                        <div className="checkout-totals1">
                          <span className="checkout-rupee">&#x20B9;</span>
                          <span className='checkout-rupee' >-50</span>
                        </div>
                      </Col>
                      <Col xs={12} md={12} mx-auto className="mb-0">
                        <img className="checkout-totalline" src={totalline_img} />
                      </Col>
                      <Col xs={12} md={11} mx-auto>
                        <p className="checkout-total2">Total</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto>
                        <div className="checkout-totals2">
                          <span className="checkout-rupee1">&#x20B9;</span>
                          <span className='checkout-rupee1' >150</span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
              <Col xs={12} md={12} mx-auto className="mb-0">
                <Button className="checkout-confirm-btn" color="sucess">
                  Confirm Order
                </Button>
              </Col>
            </CardBody>
          </Card>
        </div>
      </Col>
      < Footer />
    </Container>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { GetCheckoutReducer } = state
  return {
    GetCheckouts: GetCheckoutReducer && GetCheckoutReducer.GetCheckouts ? GetCheckoutReducer.GetCheckouts : [],
    statusCHT: GetCheckoutReducer && GetCheckoutReducer.statusCHT ? GetCheckoutReducer.statusCHT : '',

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage)