import { USER_CHECKOUT } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export  function Checkout(data) {
  return {
    type: USER_CHECKOUT.GET_LIST,
    payload: data
  }
}

export function  CheckoutRequest() {
  return {
    type: USER_CHECKOUT.REQUEST
  }
}

export function  CheckoutSuccess(response) {
  const { data, status } = response
  return {
    type: USER_CHECKOUT.SUCCESSS,
    statusDAC: status,
    messageDAC: response.message,
    Checkout: response
  }
}

export function  CheckoutError(response) {
  return {
    type: USER_CHECKOUT.ERROR,
    statusDAC: ERROR.MSG,
    messageDAC: response && response.message ? response.message : 'Something went wrong!'
  }
}