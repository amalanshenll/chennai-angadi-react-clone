import {  USER_CHECKOUT } from '../../helpers/type'

function CheckoutReducer(state = {}, action) {
  switch (action.type) {
    case  USER_CHECKOUT.REQUEST:
      return Object.assign({}, state, {
        loadingCOT: true
      });
    case  USER_CHECKOUT.SUCCESSS:
      return Object.assign({}, state, {
        loadingCOT: false,
        Checkouts: action.Checkout,
        statusCOT: action.statusCOT,
        messageCOT: action.messageCOT,
        count: action.count
      });
    case  USER_CHECKOUT.ERROR:
      return Object.assign({}, state, {
        loadingCOT: false,
        error: true,
        Checkouts: [],
        statusCOT: action.statusCOT,
        messageCOT: action.messageCOT
      });
    case  USER_CHECKOUT.CLEAR:
      return Object.assign({}, state, {
        loadingCOT: false,
        error: true,
        Checkouts: [],
        statusCOT: '',
        messageCOT: ''
      });
    default:
      return state;
  }
}

export default CheckoutReducer;
