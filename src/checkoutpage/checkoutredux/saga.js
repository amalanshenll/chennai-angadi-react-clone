import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { USER_CHECKOUT } from '../../helpers/type'
import {
  CheckoutRequest,
  CheckoutSuccess,
  CheckoutError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* CheckoutListAsync(action) {
  const params = action.payload;
  try {
    yield put(CheckoutRequest());
    const data = yield call(() => API.post(apiUrl.CHECKOUT, action.payload));
    yield put(CheckoutSuccess(data));
  } catch (error) {
    yield put(CheckoutError());
  }
}

function* fetchCheckoutRootSaga() {
  yield all([
    yield takeEvery(USER_CHECKOUT.GET_LIST, CheckoutListAsync)
  ]);
}

const fetchCheckoutSaga = [
  fork(fetchCheckoutRootSaga),
];

export default fetchCheckoutSaga;
