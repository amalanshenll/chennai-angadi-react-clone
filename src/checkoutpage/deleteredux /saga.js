import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { DELETE_ADDTOCART } from '../../helpers/type'
import {
  DeleteAddtocartRequest,
  DeleteAddtocartSuccess,
  DeleteAddtocartError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* DeleteAddtocartListAsync(action) {
  const params = action.payload;
  try {
    yield put(DeleteAddtocartRequest());
    const data = yield call(() => API.delete(apiUrl.OREDER, action.payload));
    yield put(DeleteAddtocartSuccess(data));

  } catch (error) {
    yield put(DeleteAddtocartError());
  }
}

function* fetchDeleteAddtocartRootSaga() {
  yield all([
    yield takeEvery(DELETE_ADDTOCART.GET_LIST, DeleteAddtocartListAsync)
  ]);
}

const fetchDeleteAddtocartSaga = [
  fork(fetchDeleteAddtocartRootSaga),
];

export default fetchDeleteAddtocartSaga;
