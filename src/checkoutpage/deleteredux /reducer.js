import { DELETE_ADDTOCART } from '../../helpers/type'

function DeleteAddtocartReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_ADDTOCART.REQUEST:
      return Object.assign({}, state, {
        loadingDAC: true
      });
    case DELETE_ADDTOCART.SUCCESSS:
      return Object.assign({}, state, {
        loadingDAC: false,
        DeleteAddtocarts: action.DeleteAddtocart,
        statusDAC: action.statusDAC,
        messageDAC: action.messageDAC,
        count: action.count
      });
    case DELETE_ADDTOCART.ERROR:
      return Object.assign({}, state, {
        loadingDAC: false,
        error: true,
        DeleteAddtocarts: [],
        statusDAC: action.statusDAC,
        messageDAC: action.messageDAC
      });
    case DELETE_ADDTOCART.CLEAR:
      return Object.assign({}, state, {
        loadingDAC: false,
        error: true,
        DeleteAddtocarts: [],
        statusDAC: '',
        messageDAC: ''
      });
    default:
      return state;
  }
}

export default DeleteAddtocartReducer;
