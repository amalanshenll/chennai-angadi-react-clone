import {  USER_GET_CHECKOUT } from '../../helpers/type'

function GetCheckoutReducer(state = {}, action) {
  switch (action.type) {
    case  USER_GET_CHECKOUT.REQUEST:
      return Object.assign({}, state, {
        loadingCHT: true
      });
    case  USER_GET_CHECKOUT.SUCCESSS:
      return Object.assign({}, state, {
        loadingCHT: false,
        GetCheckouts: action.GetCheckout,
        statusCHT: action.statusCHT,
        messageCHT: action.messageCHT,
        count: action.count
      });
    case  USER_GET_CHECKOUT.ERROR:
      return Object.assign({}, state, {
        loadingCHT: false,
        error: true,
        GetCheckouts: [],
        statusCHT: action.statusCHT,
        messageCHT: action.messageCHT
      });
    case  USER_GET_CHECKOUT.CLEAR:
      return Object.assign({}, state, {
        loadingCHT: false,
        error: true,
        GetCheckouts: [],
        statusCHT: '',
        messageCHT: ''
      });
    default:
      return state;
  }
}

export default GetCheckoutReducer;
