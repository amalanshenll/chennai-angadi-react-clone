import React, { useState } from 'react'
import '../landinpage/index.css'
import Termsheaders from '../header/CommonHeader'
import termunderline_img from '../assets/images/termunderline_img.png'
import Footer from '../components/Footer';
import totalline_img from '../assets/images/totalline_img.png'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardText,
  Input
} from 'reactstrap';

function PaymentMethod() {
  return (
    <Container className="themed-container" fluid={true}>
      <div>
        <Termsheaders />
      </div>
      <div className="terms-heading">
        <h1 className="terms-head">Payment</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <Col xs={12} md={12} mx-auto className="mb-5">
        <div>
          <Card className="payment-card">
            <CardTitle className="payment-card-head">
              Payment Method
            </CardTitle>
            <CardBody>
              <Row>
                <Col xs={12} md={12} className="mb-2">
                  <div className='payment-rectangle mb-3'>
                    <Col xs={12} md={5} className="mb-5">
                      <div className="payment-method">
                        <Input className="payment-radio-btn" type="radio"></Input>
                        <label className="payment-radio-text" for="male">Credit / Debit Card</label>
                      </div>
                    </Col>
                  </div>
                  <div className='payment-rectangle'>
                    <Col xs={12} md={5} className="mb-5">
                      <div className="payment-method">
                        <Input className="payment-radio-btn" type="radio"></Input>
                        <label className="payment-radio-text" for="male">Cash on Delivery</label>
                      </div>
                    </Col>
                  </div>
                </Col>
              </Row>
              <Row className="mb-4">
                <Col xs={12} md={12} mx-auto className="mb-2">
                  <h1 className="payment-order">Bill Details</h1>
                </Col>
                <Col xs={12} md={12} mx-auto>
                  <div className="payment-bill">
                    <Row>
                      <Col xs={12} md={11} mx-auto>
                        <p className="payment-total">Subtotal</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto>
                        <div className="payment-totals">
                          <span className="payment-rupee">&#x20B9;</span>
                          <span className='payment-rupee' >200</span>
                        </div>
                      </Col>
                      <Col xs={12} md={11} mx-auto>
                        <p className="payment-total1">Coupon Applied</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto className="mb-0">
                        <div className="payment-totals1">
                          <span className="payment-rupee">&#x20B9;</span>
                          <span className='payment-rupee' >-50</span>
                        </div>
                      </Col>
                      <Col xs={12} md={12} mx-auto className="mb-0">
                        <img className="payment-totalline" src={totalline_img} />
                      </Col>
                      <Col xs={12} md={11} mx-auto>
                        <p className="payment-total2">Total</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto>
                        <div className="payment-totals2">
                          <span className="payment-rupee1">&#x20B9;</span>
                          <span className='payment-rupee1' >150</span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
              <Button className="payment-confirm-btn">
                Confirm Order
              </Button>
            </CardBody>
          </Card>
        </div>
      </Col>
      < Footer />
    </Container>

  )
}
export default PaymentMethod