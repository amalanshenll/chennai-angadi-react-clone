import { GET_ADD_LIST } from '../../helpers/type'

function GetAddToCartListReducer(state = {}, action) {
  switch (action.type) {
    case GET_ADD_LIST.REQUEST:
      return Object.assign({}, state, {
        loadingGetAdd: true
      });
    case GET_ADD_LIST.SUCCESSS:
      return Object.assign({}, state, {
        loadingGetAdd: false,
        GetAddToCartLists: action.GetAddToCartList,
        statusGetAdd: action.statusGetAdd,
        messageGetAdd: action.messageGetAdd,
        nameGetAdd: action.nameGetAdd,
        count: action.count
      });
    case GET_ADD_LIST.ERROR:
      return Object.assign({}, state, {
        loadingGetAdd: false,
        error: true,
        GetAddToCartLists: [],
        statusGetAdd: action.statusGetAdd,
        messageGetAdd: action.messageGetAdd,
        nameGetAdd: action.nameGetAdd,
      });
    case GET_ADD_LIST.CLEAR:
      return Object.assign({}, state, {
        loadingGetAdd: false,
        error: true,
        GetAddToCartLists: [],
        statusGetAdd: '',
        messageGetAdd: '',
        nameGetAdd:''
      });
    default:
      return state;
  }
}

export default GetAddToCartListReducer;
