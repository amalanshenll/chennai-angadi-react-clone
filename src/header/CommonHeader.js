import React, { useEffect, useRef, useState } from 'react'
import '../landinpage/index.css'
import '../Addressprofile/addressprofile.scss'
import logo_img from '../assets/images/logo_img.png'
import { FaShoppingCart } from "react-icons/fa"
import { FiSearch } from "react-icons/fi"
import { CartSideBar, EmptyCart } from './Styles'
import { GetAddToCartList } from './getaddtocartredux/action'
import { connect } from 'react-redux'
import _ from 'lodash'
import { Link } from 'react-router-dom'
import {
  STATUS_RESPONSE
} from '../helpers/const'
import useOnClickOutside from './OutsideClick';
import CardMap from '../categoriepage/CardLoopings'
import Mask_Group from '../assets/images/mask_Group.png'
import { DeleteAddtocart } from '../checkoutpage/deleteredux /action'
import { Checkout } from '../checkoutpage/checkoutredux/action'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  CardImg,
  CardTitle,
  CardBody,
  Alert
} from 'reactstrap';

function Termsheaders(props) {

  const {
    ProductsLists,
    dispatch,
    contains,
    statusPR,
    GetAddToCartLists,
    match,
    statusDAC
  } = props

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [addtocartvisible, setAddtocartvisible] = useState(false);
  const [isToggle, setToggle] = useState(false);
  const toggle = () => setDropdownOpen(prevState => !prevState);
  const [isOpen, setIsOpen] = useState(true);
  const toggles = () => setIsOpen(!isOpen);
  const ref = useRef();
  useOnClickOutside(ref, () => setToggle(false));
  const [toggleAdd, setToggleAdd] = useState(false)

  function Addtocart() {
    dispatch(GetAddToCartList());
  }

  function onDeleteItem(id) {
    dispatch(DeleteAddtocart({ productId: id }));
  }

  function onCheckout(id, deliverycharge) {
    dispatch(Checkout({
      productId: id,
      delivery_charge: deliverycharge
    }));
  }

  useEffect(() => {
    if (statusDAC === STATUS_RESPONSE.SUCCESS_MSG) {
      setToggleAdd(true)
      const timer = setTimeout(() => {
        setToggleAdd(false)
      }, 3000);
    }
  }, [statusDAC])

  return (
    <Container className="themed-container" fluid={true}>
      <Col sm="12">
        <div className="terms-header-bg" >
          <Navbar color="faded" variant="dark" light expand="md">
            <Col sm="2">
              <img className='terms-logo-img' src={logo_img} width="120" height="110" />
            </Col>
            <NavbarToggler onClick={toggles} />
            <Collapse isOpen={isOpen} navbar>
              <Nav className='terms-navbar' >
                <NavItem>
                  <NavLink className="terms-navlink">Home</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="terms-nav-wishlistbtn">My Wish List</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="terms-navlink">Order History</NavLink>
                </NavItem>
                <NavItem>
                  <Link className="terms-navlink" to={`/myprofile/$`} >My Profile</Link>
                </NavItem>
                <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                  <DropdownToggle caret className="setting-drpdn">
                    Settings
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem >Header</DropdownItem>
                    <DropdownItem>Some Action</DropdownItem>
                    <DropdownItem>Foo Action</DropdownItem>
                    <DropdownItem>Bar Action</DropdownItem>
                    <DropdownItem>Quo Action</DropdownItem>
                  </DropdownMenu>
                </Dropdown>
                <NavItem >
                  <Link
                    onClick={Addtocart}
                  >
                    <div className="terms-nav-cart" size="sm"
                    > <FaShoppingCart onClick={() => setToggle(true)}
                      /></div>
                  </Link>
                </NavItem>
                <Col className="terms-search">
                  <Input className=" terms-search form-control-md " type="text" placeholder="Search" id="inputID">
                  </Input>
                  <span className="terms-search-icon "><FiSearch /></span>
                </Col>
              </Nav>
            </Collapse>
          </Navbar>
          {
            isToggle ?
              <div ref={ref} >
                <CartSideBar
                  isToggle={isToggle}
                  setToggle={setToggle}
                >
                  {
                    GetAddToCartLists && GetAddToCartLists.map((item, i) => {
                      return (
                        <div key={item, i}>
                          <Row>
                            <Col xs={12} md={8} className="mb-2">
                              <h5>Add to Cart</h5>
                              <Card>
                                <Col xs={12} md={12} className="mb-2">
                                  <CardImg className="addtocart-img" src={Mask_Group} alt="Card image cap" />
                                </Col>
                                <CardTitle className='addtocart-text'>
                                  MUSHROOM
                                </CardTitle>
                                <CardBody>
                                  <div className="addtocart-amountdetail">
                                    <span class="addtocart-webrupee">&#x20B9;</span>
                                    <span className='addtocart-amount'>
                                      100
                                    </span>
                                  </div>
                                  <h6 className="addtocart-deliverycharge">
                                    delivery_charge:
                                    {
                                      item.delivery_charge
                                    }
                                  </h6>
                                  <h6 className="addtocart-price">
                                    order_price:
                                    {
                                      item.order_price
                                    }
                                  </h6>
                                  <h6 className="addtocart-packagecharge">
                                    package_charge:
                                    {
                                      item.package_charge
                                    }
                                  </h6>
                                  <Alert color="success" isOpen={toggleAdd} fade={false}
                                  >
                                    Product Deleted  successfully
                                  </Alert>
                                  <Button
                                    className='addtocart-delete-btn'
                                    onClick={() => onDeleteItem(item._id)}
                                  >Remove</Button>
                                  <Link to={`/checkout/$`} >
                                    <Button
                                      className='addtocart-Buy-btn'
                                      onClick={() => onCheckout(item._id)}
                                    >
                                      proceed to Buy
                                    </Button>
                                  </Link>
                                </CardBody>
                              </Card>
                            </Col>
                          </Row>
                        </div>
                      )
                    })
                  }
                </CartSideBar>
              </div>
              : <EmptyCart> empty cart</EmptyCart>
          }
        </div>
      </Col>
    </Container>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { GetAddToCartListReducer, DeleteAddtocartReducer } = state
  return {
    GetAddToCartLists: GetAddToCartListReducer && GetAddToCartListReducer.GetAddToCartLists ? GetAddToCartListReducer.GetAddToCartLists : [],
    statusPR: GetAddToCartListReducer && GetAddToCartListReducer.statusPR ? GetAddToCartListReducer.statusPR : '',
    statusDAC: DeleteAddtocartReducer && DeleteAddtocartReducer.statusDAC ? DeleteAddtocartReducer.statusDAC : [],
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Termsheaders)
