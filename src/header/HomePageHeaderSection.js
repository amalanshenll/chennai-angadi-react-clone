import React, { useState, useEffect } from 'react';
import '../landinpage/index.css'
import '../myprofile/myprofile.scss'
import loinlogo_img from '../assets/images/loinlogo_img.png'
import logo_img from '../assets/images/logo_img.png'
import loginunderline_img from '../assets/images/loginunderline_img.png'
import smallunderline_img from '../assets/images/smallunderline_img.png'
import facebook_img from '../assets/images/facebook_img.png'
import { AiFillGoogleCircle } from "react-icons/ai"
import { FiEdit } from "react-icons/fi"
import { connect } from 'react-redux'
import { checkuserUniqueMobileNumber, fetchList } from '../header/redux/action'
import { LOGIN_SERVER_ERRS, } from '../helpers/type'
import { REGISTERATION_FORM_MSGS, LOGIN_MGS } from '../helpers/message'
import { verifyOtp } from './OTPredux/action '
import { loginList } from './loginredux/action'
import { FiSearch } from "react-icons/fi"
import { Link } from 'react-router-dom'
import {MyWishList} from '../mywishlist/mywishlistredux/action'
import {
  LOCAL_STORAGE,
  REGEX,
  STATUS_RESPONSE,
  VERIFY_MESSAGE_RESPONSE,
  JWT_EXPIRED,
} from '../helpers/const'
import {
  Alert,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

function Homepageheader(props) {

  const {
    dispatch,
    statusFL,
    messageVO,
    statusLG,
    billionaireloginList,
    billionairecategorieList
  } = props
  
  const [formData, setFormData] = useState({
    email: '',
    mobile_number: ''
  })

  const [verifyData, setVerifyData] = useState({
    mobile_number: '',
    code: '',
    type:'LOGIN' || 'SIGN_UP'
  })

  const [loginData, setLoginData] = useState({
    mobile_number: ''
  })
  const [errors, setErrors] = useState({})
  const [data, setData] = useState("")
  const [visible, setVisible] = useState(false);
  const [signupvisible, setSignupvisible] = useState(false);
  const [loginvisible, setLoginvisible] = useState(false);
  const [loginnumbervisible, setLoginnumbervisible] = useState(false);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const dropdowntoggle = () => setDropdownOpen(prevState => !prevState);

  const validate = () => {
    let valid = true
    const errors = {}
    if (!formData.mobile_number) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.USER_MOBILENUMBER_REQUIRED
      valid = false
    } else if (!REGEX.MOBILE.test(formData.mobile_number)) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.MOBILE_NUMBER_INVALID
      valid = false
    }
    if (!formData.email) {
      errors.email = REGISTERATION_FORM_MSGS.USER_EMAIL_REQUIRED
      valid = false
    } else if (!REGEX.EMAIL.test(formData.email)) {
      errors.email = REGISTERATION_FORM_MSGS.EMAIL_INVALID
      valid = false
    }
    setErrors(errors)
    return valid
  }

  const verifyvalidate = () => {
    let valid = true
    const errors = {}
    if (!verifyData.mobile_number) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.USER_MOBILENUMBER_REQUIRED
      valid = false
    } else if (!REGEX.MOBILE.test(verifyData.mobile_number)) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.MOBILE_NUMBER_INVALID
      valid = false
    }
    if (!verifyData.code) {
      errors.code = LOGIN_MGS.OTP_REQUIRED
      valid = false
    } else if (!REGEX.OTP.test(verifyData.code)) {
      errors.code = LOGIN_MGS.OTP_INVALID
      valid = false
    }
    if (!verifyData.type) {
      errors.type = LOGIN_MGS.TYPE_REQUIRED
      valid = false
    }
    setErrors(errors)
    return valid
  }

  const loginvalidate = () => {
    let valid = true
    const errors = {}
    if (!loginData.mobile_number) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.USER_MOBILENUMBER_REQUIRED
      valid = false
    } else if (!REGEX.MOBILE.test(loginData.mobile_number)) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.MOBILE_NUMBER_INVALID
      valid = false
    }
    setErrors(errors)
    return valid
  }

  const {
    Logins,
    classNames
  } = props;
  const [modals, setModals] = useState(false);
  const toggless = () => setModals(!modals);

  const {
    login,
    classNamesignup
  } = props;
  const [modalss, setModalss] = useState(false);
  const togglesignup = () => setModalss(!modalss);

  const {
    Login,
    className
  } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const hide = () => setModal(false);

  const [isOpen, setIsOpen] = useState(true);

  const toggles = () => setIsOpen(!isOpen);

  function handleChange(e) {
    const { name, value } = e.target;
    setFormData(formData => ({ ...formData, [name]: value }));
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (validate()) {
      dispatch(fetchList(formData))
    }
  }

  function verifyChange(e) {
    const { name, value } = e.target;
    setVerifyData(verifyData => ({ ...verifyData, [name]: value }));
  }

  function verifySubmit(e) {
    e.preventDefault();
    if (verifyvalidate()) {
      dispatch(verifyOtp(verifyData))
    }
  }

  function loginChange(e) {
    const { name, value } = e.target;
    setLoginData(loginData => ({ ...loginData, [name]: value }));
  }

  function loginSubmit(e) {
    e.preventDefault();
    if (loginvalidate()) {
      dispatch(loginList(loginData))
    }
  }

  function wishbtn () {
    dispatch(MyWishList())
  }

  useEffect(() => {
    if (statusFL === STATUS_RESPONSE.SUCCESS_MSG) {
      setModals(true);
      setModalss(false);
    } else if (statusFL === STATUS_RESPONSE.ERROR) {
      setVisible(true);
      const timer = setTimeout(() => {
        setVisible(false);
      }, 5000);
      return () => clearTimeout(timer);
    }
  }, [statusFL])

  useEffect(() => {
    if (messageVO === VERIFY_MESSAGE_RESPONSE.SIGNUP_SUCCESS_MSG) {
      setSignupvisible(true);
      setModals(false);
      const timer = setTimeout(() => {
        setSignupvisible(false);
      }, 5000);
    } else if (messageVO === VERIFY_MESSAGE_RESPONSE.LOGIN_SUCCESS_MSG) {
      setModals(false);
      setModal(false);
      setLoginvisible(true);
      const timer = setTimeout(() => {
        setLoginvisible(false);
      }, 5000);
      return () => clearTimeout(timer);
    }
  }, [messageVO])

  useEffect(() => {
    if (statusLG === STATUS_RESPONSE.SUCCESS_MSG) {
      setModals(true);
      setModalss(false);
    } else if (statusLG === STATUS_RESPONSE.ERROR) {
      setLoginnumbervisible(true);
      const timer = setTimeout(() => {
        setLoginnumbervisible(false);
      }, 4000);
      return () => clearTimeout(timer);
    }
  }, [statusLG])

  return (
    <Container className="themed-container" fluid={true}>
      <Col sm="12">
              {
                billionaireloginList ?  
                <Navbar color="faded" variant="dark" light expand="md">
                <Col sm="2">
                  <img className='terms-logo-img' src={logo_img} width="120" height="110" />
                </Col>
                <NavbarToggler onClick={toggles} />
                <Collapse isOpen={isOpen} navbar>
                  <Nav className='terms-navbar' >
                    <NavItem>
                      <NavLink className="terms-navlink">Home</NavLink>
                    </NavItem>
                    <NavItem>
                    <Link to={`/wishlist/$`} onClick={wishbtn} style={{ textDecoration: 'none' }}  className="hompage-navlink-btn"
                    >My Wish List</Link>
                    </NavItem>
                    <NavItem>
                      <NavLink className="terms-navlink">Order History</NavLink>
                    </NavItem>
                    <NavItem>
                      <Link to={`/myprofile/$`} className="terms-navlink"  style={{ textDecoration: 'none' }}
                      >My Profile</Link>
                    </NavItem>
                    <Dropdown isOpen={dropdownOpen} toggle={dropdowntoggle}>
                      <DropdownToggle caret className="setting-drpdn">
                        Settings
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem >Logout</DropdownItem>
                        <DropdownItem>Some Action</DropdownItem>
                        <DropdownItem>Foo Action</DropdownItem>
                        <DropdownItem>Bar Action</DropdownItem>
                        <DropdownItem>Quo Action</DropdownItem>
                      </DropdownMenu>
                    </Dropdown>
                    <Col className="terms-search">
                      <Input className=" terms-search form-control-md " type="text" placeholder="Search" id="inputID">
                      </Input>
                      <span className="terms-search-icon "><FiSearch /></span>
                    </Col>
                  </Nav>
                </Collapse>
              </Navbar>
              :
              <div className="navbar-bg-img" >
              <Navbar color="faded" variant="dark" light expand="md" >
                <Col sm="2">
                  <img className='logo-img' src={logo_img} width="120" height="110" />
                </Col>
                <NavbarToggler onClick={toggles} />
                <Collapse isOpen={isOpen} navbar>
              <Nav className='homepage-navbar' >
              <NavItem>
                <NavLink className="hompage-nav-homebtn">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="hompage-navlink-btn">My Wish List</NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="hompage-navlink-btn">Order History</NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="hompage-navlink-btn">Settings</NavLink>
              </NavItem>
              <NavItem >
                <Button className="hompage-nav-loginbtn" size="sm" outline color="olivedrab"
                  onClick={toggle}>Login</Button>
              </NavItem>
              <Col>
                <NavItem className='sign-btn'>
                  <Button className="nav-sign-btn" outline color="olivedrab" size="sm"
                    onClick={togglesignup}>Sign Up</Button>
                </NavItem>
              </Col>
            </Nav>

            </Collapse>
          </Navbar>
        </div>
              }
      </Col>
      <div>
        <Modal
          isOpen={modal}
          toggle={toggle}
          className={className}
          size="md"
          className="login-page"
          centered
        >
          <div>
            <Alert color="danger" isOpen={loginnumbervisible} fade={false}>
              YOUR NUMBER IS NOT VALID
            </Alert>
          </div>
          <img className='login-logo-img' src={loinlogo_img} />
          <img className='login-under-line' src={loginunderline_img} />
          <h3 className="login">Login</h3>
          <div className="login-input">
            <Input
              className="login-number"
              type="mobilenumber"
              name="mobile_number"
              placeholder="Mobile Number"
              value={loginData.mobile_number}
              onChange={loginChange}
              maxLength="10"
            />
            {
              errors.mobile_number && (<div className='text-danger'> {errors.mobile_number} </div>)
            }
          </div>
          <div className='login-button mb-2'>
            <Button
              className='login-btn'
              color="olivedrab"
              size="md"
              onClick={loginSubmit}
            >
              Login
            </Button>{' '}
          </div>
          <div>
            <Row>
              <Col sm="12" md="12" className="login-social mb-1">
                <img className='login-ul-img' src={smallunderline_img} />
                <span className="social">Social Login</span>
                <img className='login-ul-img1' src={smallunderline_img} />
              </Col>
            </Row>
          </div>
          <div>
            <Row>
              <Col sm="12" md="12" className="login-img mb-0">
                <img className='facebook' src={facebook_img} />
                <span className='search'><AiFillGoogleCircle /></span>
              </Col>
            </Row>
          </div>
          <div>
            <Row>
              <Col sm="12" md="12" className='login-lastline'>
                <p className="account-log">Don't have account? <span className='signup-para'>SIGN UP</span></p>
              </Col>
            </Row>
          </div>
        </Modal>
      </div>
      <div>
        <div>
          <Alert color="success" isOpen={signupvisible} fade={false}>
            SIGNUP SUCCESSFULLY
          </Alert>
          <Alert color="success" isOpen={loginvisible} fade={false}>
            LOGIN SUCCESSFULLY
          </Alert>
        </div>
        <Modal
          isOpen={modals}
          toggle={toggless}
          className={classNames}
          size="md"
          centered
        >
          <img className='otp-logo-img' src={loinlogo_img} />
          <img className='login-under-line' src={loginunderline_img} />
          <h3 className="login">OTP</h3>
          <p className="otp-para">OTP sent to your mobile number</p>
          <div>
            <Row>
              <Col sm="12" md="12" className=" mb-2">
                <div className="otp-numbers">9876543210 <span className="otp-edit"><FiEdit /></span> </div>
              </Col>
            </Row>
          </div>
          <div className="otp-input">
            <Input
              className="otp-number"
              type="code"
              name="code"
              placeholder="Enter Otp"
              value={verifyData.code}
              onChange={verifyChange}
              maxLength="6"
            />
            {
              errors.code && (<div className='text-danger'>{errors.code}</div>)
            }
          </div>
          <div className="login-input">
            <Input
              className="login-number"
              type="mobile_number"
              name="mobile_number"
              placeholder="Mobile Number"
              value={verifyData.mobile_number}
              onChange={verifyChange}
              maxLength="10"
            />
            {
              errors.mobile_number && (<div className='text-danger'> {errors.mobile_number} </div>)
            }
          </div>
          <div className="login-input">
          </div>
          <div className='login-button mb-3'>
            <Button
              className='login-btn'
              color="success"
              size="md"
              onClick={verifySubmit}
            >
              Verify
            </Button>{' '}
          </div>
          <p className="resend-otp">Resend OTP</p>
        </Modal>
      </div>
      <div>
        <Modal
          isOpen={modalss}
          toggle={togglesignup}
          className={classNamesignup}
          size="md"
          className="login-page"
          centered
        >
          <div>
            <Alert color="danger" isOpen={visible} fade={false}>
              ALREADY PHONENUMBER EXIST
            </Alert>
          </div>
          <img className='login-logo-img' src={loinlogo_img} />
          <img className='login-under-line' src={loginunderline_img} />
          <h3 className="login">Sign Up</h3>
          <form>
            <div className="login-input">
              <Input
                className="login-number"
                type="mobilenumber"
                name="mobile_number"
                placeholder="Mobile Number"
                value={formData.mobile_number}
                onChange={handleChange}
                maxLength="10"
                />
              {
                errors.mobile_number && (<div className='text-danger'>{errors.mobile_number}</div>)
              }
            </div>
            <div className="login-input">
              <Input
                className="login-number"
                type="email"
                name="email"
                placeholder="Email Id"
                value={formData.email}
                onChange={handleChange}
              />
              {
                errors.email && (<div className='text-danger'>{errors.email}</div>)
              }
            </div>
            <div className='login-button mb-2'>
              <Button className='login-btn' color="olivedrab" size="md"
                onClick={handleSubmit}
              >
                sign up
              </Button>{' '}
            </div>
          </form>
          <div>
            <Row>
              <Col sm="12" md="12" className="login-social mb-1">
                <img className='login-ul-img' src={smallunderline_img} />
                <span className="social">Social Login</span>
                <img className='login-ul-img1' src={smallunderline_img} />
              </Col>
            </Row>
          </div>
          <div>
            <Row>
              <Col sm="12" md="12" className="login-img mb-0">
                <img className='facebook' src={facebook_img} />
                <span className='search'><AiFillGoogleCircle /></span>
              </Col>
            </Row>
          </div>
          <div>
            <Row>
              <Col sm="12" md="12" className='login-lastline'>
                <p className="account-log">Already have account? <span className='signup-para'>Login</span></p>
              </Col>
            </Row>
          </div>
        </Modal>
      </div>
    </Container>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { fetchUserListReducer, fetchOTPListReducer, loginListReducer,fetcCategorigeListReducer } = state
  return {
    billionaireuserList: fetchUserListReducer ? fetchUserListReducer.billionaireuserList : [],
    billionaireotpList: fetchOTPListReducer ? fetchOTPListReducer.billionaireotpList : [],
    billionaireloginList: loginListReducer ? loginListReducer.billionaireloginList : [],
    statusFL: fetchUserListReducer ? fetchUserListReducer.statusFL : '',
    messageVO: fetchOTPListReducer ? fetchOTPListReducer.messageVO : '',
    statusLG: loginListReducer ? loginListReducer.statusLG : '',
    billionairecategorieList: fetcCategorigeListReducer ? fetcCategorigeListReducer.billionairecategorieList : [],
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Homepageheader)