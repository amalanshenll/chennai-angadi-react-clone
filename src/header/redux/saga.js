import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { USER } from '../../helpers/type'
import {
  fetchListRequest,
  fetchListSuccess,
  fetchListError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* fetchListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(fetchListRequest());
    const data = yield call(() => API.post(apiUrl.USER,  params ));
    yield put(fetchListSuccess(data));
  } catch (error) {
    yield put(fetchListError());
  }
}

function* fetchListRootSaga() {
  yield all([
    yield takeEvery(USER.GET_LIST, fetchListAsync)
  ]);
}

const fetchListSaga = [
  fork(fetchListRootSaga),
];

export default fetchListSaga;
