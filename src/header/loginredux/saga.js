import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { LOGIN } from '../../helpers/type'
import {
  loginListRequest,
  loginListSuccess,
  loginListError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* fetchListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(loginListRequest());
    const data = yield call(() => API.post(apiUrl.LOGIN,  params ));
    yield put(loginListSuccess(data));
  } catch (error) {
    yield put(loginListError());
  }
}

function* fetchListRootSaga() {
  yield all([
    yield takeEvery(LOGIN.GET_LIST, fetchListAsync)
  ]);
}

const loginListSaga = [
  fork(fetchListRootSaga),
];

export default loginListSaga;
