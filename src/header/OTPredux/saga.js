import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { VERIFY_OTP } from '../../helpers/type'
import {
  verifyOtpRequest,
  verifyOtpSuccess,
  verifyOtpError
} from './action ';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* fetchListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(verifyOtpRequest());
    const data = yield call(() => API.post(apiUrl.OTP,  params ));
    yield put(verifyOtpSuccess(data));
  } catch (error) {
    yield put(verifyOtpError());
  }
}

function* fetchOTPRootSaga() {
  yield all([
    yield takeEvery(VERIFY_OTP.VERIFY_DATA, fetchListAsync)
  ]);
}

const fetchOTPSaga = [
  fork(fetchOTPRootSaga),
];

export default fetchOTPSaga;
