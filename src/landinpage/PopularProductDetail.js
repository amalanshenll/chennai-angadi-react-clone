import React, { useState, useEffect } from 'react'
import './index.css';
import popular_img from '../assets/images/popular_img.png'
import Heart from "react-heart"
import {
  Button,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody
} from 'reactstrap';

import { connect } from 'react-redux'
import _ from 'lodash'
import { Link } from 'react-router-dom'

function Products(props) {

  const {
    ProductsLists,
    dispatch,
    statusPR,
  } = props

  const [active, setActive] = useState(false)


  return (
    <div>
      {
        ProductsLists && ProductsLists.map((item, i) => {
          return (
            <div>
              <Card className='popularcard'>
                <CardImg className="popular-card-img ml-auto" src={popular_img} alt="Card image cap" />
                <CardBody>
                  <CardTitle tag="h5" className="popular-car-dhead mb-2">
                    {
                      item.name
                    }
                  </CardTitle>
                  <Row>
                    <Col xs={12} md={12} className="product-amt mb-3" >
                      <span class="categires-webrupee">&#x20B9;</span>
                      <span className='amount' >
                        {
                          item.price
                        }
                      </span>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} md={5}>
                      <span><Button className='popular-cart' size="md" >Add To Cart</Button></span>
                    </Col>
                    <Col xs={12} md={5}>
                      <span><Button className='popular-like' outline color=" btn-outline-success" size="sm">
                        <div className="img-fluid">
                          <Heart isActive={active} onClick={() => setActive(!active)} animationTrigger="both" inactiveColor="#83A43E" activeColor="#83A43E" animationDuration={0.1} />
                        </div>
                      </Button></span>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </div>
          )
        })
      }
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { ProductsListReducer } = state
  return {
    ProductsLists: ProductsListReducer && ProductsListReducer.ProductsLists ? ProductsListReducer.ProductsLists : [],
    statusPR: ProductsListReducer && ProductsListReducer.statusPR ? ProductsListReducer.statusPR : '',

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)