import React, { useState, useEffect } from 'react'
import './index.css';
import clientveg_img from '../assets/images/clientveg_img.png'
import clientqtn from '../assets/images/clientqtn.png'
import clientside_img from '../assets/images/clientside_img.png'
import client from '../assets/images/client.png'


import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardText
} from 'reactstrap';

function Client() {
  return (
    <Container className='client-container' fluid={true}>
      <div className='client-bg'>
        <Row>
          <Col xs={12} md={2} mx-auto>
            <div className="" >
              <img src={clientqtn} alt='image' className="img-fluid " />
            </div>
          </Col>
          <Col xs={12} md={6} mx-auto className='client-card ml-auto'>
            <Row>
              <Col xs={12} md={12} mx-auto>
                <h1 className='client-head mb-3'>Our Client Say!</h1>
                <p className='client-head mb-5'>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                <Col xs={12} md={12} mx-auto>
                  <div>
                    <Card>
                      <Col xs={12} md={12} mx-auto className="client-photo">
                        <img className='customer-img' src={client} />
                        <span><h7 className='client-merry'>Mery Walter
                          </h7>
                          <Row className='customer-name'>
                            <Col xs={12} md={12} mx-auto>
                              <h7 className="client-analyst">Food Anlayst</h7>
                            </Col>
                          </Row>
                        </span>
                      </Col>
                      <CardBody>
                        <CardText className="clien-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                        galley of type and scrambled it to make a type specimen book.
                        </CardText>
                      </CardBody>
                    </Card>
                  </div>
                </Col>
              </Col>
            </Row>
          </Col>
          <Col xs={12} md={3}>
            <div className="client-side-img" >
              <img src={clientveg_img} alt='image' />
            </div>
          </Col>
          <Col xs={12} md={2} mx-auto>
            <div className="client-under-img" >
              <img src={clientside_img} alt='image' className="client-under-img" />
            </div>
          </Col>
        </Row>
      </div>
    </Container>
  )
}
export default Client