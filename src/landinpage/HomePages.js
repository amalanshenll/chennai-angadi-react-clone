import React, { useState } from 'react'
import './index.css'
import Categories from '../categoriepage/Categorie'
import Footer from '../components/Footer';
import Client from './Clients'
import LikeProduct from './Populars'
import Homepageheader from '../header/HomePageHeaderSection'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card
} from 'reactstrap';

function Homepage() {
  const [isOpen, setIsOpen] = useState(true);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <Container className="themed-container" fluid={true}>
      <div className="banersection-bg">
      <Homepageheader/>
          <Col xs={5} md={6}>
            <div className="first-head" >
              <h1 className="banersection-buy">Buy More & </h1>
              <h1 className="banersection-head"> Save More</h1>
              <Col xs={12} md={11}>
                <div className="banersection-head-para">
                  <p className="banersection-para2">It is a long established fact that a reader will be </p>
                  <p className="banersection-para1">distracted by the readable content of a</p>
                  <p className="banersection-para" >Page when looking at its layout</p>
                </div>
              </Col>
            </div>
            <Col xs={5} md={6}>
              <div className="view-btn">
                <Button className="banersection-view-btn">View More</Button>{' '}
              </div>
            </Col>
          </Col>
        <div className='card-section-img'>
          <Row>
            <Col xs={6} md={4}>
              <div className="baner-card-img">
                <Row>
                  <Col xs={6} md={10}>
                    <Button className="buymore-btn">Buy more & Save more
                    <br />
                      <span className='fruits-veg-btn'>Fruits and Vegetables</span>
                    </Button>{' '}
                  </Col>
                  <Col xs={6} md={12}>
                    <Button className="viewmore-btn">View More
              </Button>{' '}
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xs={6} md={4}>
              <div className="baner-card-img1">
                <Row>
                  <Col xs={6} md={10}>
                    <Button className="buymore-btn">Buy more & Save more
                    <br />
                      <span className='fruits-veg-btn'>Fruits and Vegetables</span>
                    </Button>{' '}
                  </Col>
                  <Col xs={6} md={12}>
                    <Button className="viewmore-btn">View More
              </Button>{' '}
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xs={6} md={4}>
              <div className="baner-card-img2">
                <Row>
                  <Col xs={6} md={10}>
                    <Button className="buymore-btn">Buy more & Save more
                    <br />
                      <span className='fruits-veg-btn'>Fruits and Vegetables</span>
                    </Button>{' '}
                  </Col>
                  <Col xs={6} md={12}>
                    <Button className="viewmore-btn">View More
              </Button>{' '}
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <Categories />
      <Client />
      <LikeProduct />
      <Footer />
    </Container>
  )
}
export default Homepage

