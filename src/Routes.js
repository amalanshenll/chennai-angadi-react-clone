import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Addressprofile from './Addressprofile/addressprofile'
import Homepage from './landinpage/HomePages'
import Productdetailpages from './productsdetail/ProductDetails'
import Userprofile from './Userprofile/Userprofile'
import TermsOfuse from './termsofuse/TermsOfUse'
import Wishlist from './mywishlist/MyWishLists'
import Myprofile from './myprofile/myprofile'
import CheckoutPage from './checkoutpage/Checkout'
import Orderhistorypage from './orderhistory/OrderHistory'
import PaymentMethod from './paymentpage/Payment'
import SummaryPage from './ordersummary/OrderSummary'

function Routes(props) {
  return (
    <Router>
      <Switch>
        <Route path='/addressprofile/:id' component={Addressprofile} />
        <Route path='/addressprofile' component={Addressprofile} />
        <Route exact path='/' component={Homepage} />
        <Route path='/productdetail/:id' component={Productdetailpages} />
        <Route path='/userprofile/:id' component={Userprofile} />
        <Route path='/userprofile' component={Userprofile} />
        <Route path='/termsofuse' component={TermsOfuse} />
        <Route path='/wishlist/:id' component={Wishlist} />
        <Route path='/myprofile' component={Myprofile} />
        <Route path='/checkout' component={CheckoutPage} />
        <Route path='/orderhistory' component={Orderhistorypage} />
        <Route path='/payment' component={PaymentMethod} />
        <Route path='/ordersummary' component={SummaryPage} />
      </Switch>
    </Router>
  )
}
export default Routes;