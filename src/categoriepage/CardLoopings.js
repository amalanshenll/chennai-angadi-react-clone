import React, { useState, useEffect, useCallback } from 'react'
import '../landinpage/index.css';
import Mask_Group from '../assets/images/mask_Group.png'
import like_img from '../assets/images/like_img.png'
import { WishList } from "../mywishlist/redux/action"
import Heart from "react-heart"
import {
  Button,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  Alert
} from 'reactstrap';
import { connect } from 'react-redux'
import _ from 'lodash'
import { Link } from 'react-router-dom'
import {
  WISHLIST_MESSAGE_RESPONSE,
  STATUS_RESPONSE
} from '../helpers/const'
import { AddToCartList } from './addtocartredux/action'
import { CheckBoxOutlineBlankTwoTone } from '@material-ui/icons';
function CardMap(props) {

  const {
    ProductsLists,
    dispatch,
    messageWL,
    statusPR,
    statusADD
  } = props
  const [toggleHeart, setToggleHeart] = useState(false)
  const [toggleremoveHeart, setToggleremoveHeart] = useState(false)
  const [toggleAdd, setToggleAdd] = useState(false)
  const [toggleAdderror, setToggleAdderror] = useState(false)
  const [active, setActive] = useState(false)
  const [product_count, setProduct_count] = useState(0);

  function WishlistClick(id) {
    dispatch(WishList({ productId: id }))
  }

  useEffect(() => {
    if (messageWL === WISHLIST_MESSAGE_RESPONSE.ADD_SUCCESS_MSG) {
      setToggleHeart(true)
      const timer = setTimeout(() => {
        setToggleHeart(false)
      }, 3000);
    } else if (messageWL === WISHLIST_MESSAGE_RESPONSE.REMOVE_SUCCESS_MSG) {
      setActive(false)
      setToggleremoveHeart(true)
      const timer = setTimeout(() => {
        setToggleremoveHeart(false)
      }, 3000);
    }
  }, [messageWL])

  function Adddtocart(id, count) {
    dispatch(AddToCartList({
      productId: id,
      product_count: (product_count + 1)
    }))
  }

  useEffect(() => {
    if (statusADD === STATUS_RESPONSE.SUCCESS_MSG) {
      setToggleAdd(true)
      const timer = setTimeout(() => {
        setToggleAdd(false)

      }, 4000);
      return () => clearTimeout(timer);
    } else if (statusADD === STATUS_RESPONSE.ERROR) {
      setToggleAdderror(true)
      const timer = setTimeout(() => {
        setToggleAdderror(false)
      }, 4000);
      return () => clearTimeout(timer);
    }
  }, [statusADD])

  return (
    <div>
      {
        ProductsLists && ProductsLists.map((item, i) => {
          return (
            <div key={i}>
              <Card>
                <Col xs={12} md={12} className="mb-2">
                  <CardImg className="Categories-card-img" src={Mask_Group} alt="Card image cap" />
                </Col>
                <CardBody>
                  <Row>
                    <Col xs={12} md={12} className="kilogram-btn mb-3">
                      <Button size='sm' className="categorie-kgbtn">0.5 kg</Button>{' '}
                      <Button outline color="olivedrab" size='sm' className="categorie-kgbtn1">1 kg</Button>{' '}
                      <Button outline color="olivedrab" size='sm' className="categorie-kgbtn1">2 kg</Button>{' '}
                    </Col>
                  </Row>
                  <Link to={`/productdetail/${item._id}`} tag="h5" className="categories-card-text mb-2"
                    style={{ textDecoration: 'none' }}
                  >
                    {
                      item.name
                    }
                  </Link>
                  <Row>
                    <Col xs={12} md={12} className="categires-amount mb-2" >
                      <span class="categires-webrupee">&#x20B9;</span>
                      <span className='amount'>
                        {
                          item.price
                        }
                      </span>
                    </Col>
                  </Row>
                  <Row>
                    <Alert color="success" isOpen={toggleAdd} fade={false}>
                      ADDTOCART added  successfully
                    </Alert>
                    <Alert color="danger" isOpen={toggleAdderror} fade={false}>
                      Please Login && Login Authority only
                    </Alert>
                    <Alert color="success" isOpen={toggleHeart} fade={false}>
                      Product added to wishlist successfully
                    </Alert>
                    <Alert color="danger" isOpen={toggleremoveHeart} fade={false}>
                      Product removed from wishlist successfully
                    </Alert>
                    <Col xs={12} md={7}>
                      <span><Button className='cart' color="success" size="md"
                        onClick={() => Adddtocart(item._id)}
                      >Add To Cart</Button></span>
                    </Col>
                    <Col xs={12} md={4}>
                      <span ><Button
                        className='like'
                        outline color="success" size="sm"
                        onClick={() => WishlistClick(item._id)}
                      >
                        <div className="img-fluid">
                          <Heart isActive={active} onClick={() => setActive(!active)} animationTrigger="both" inactiveColor="#83A43E" activeColor="#83A43E" animationDuration={0.1} />
                        </div>
                      </Button></span>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </div>
          )
        })
      }
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { ProductsListReducer, WishListReducer,AddToCartListReducer } = state
  return {
    ProductsLists: ProductsListReducer && ProductsListReducer.ProductsLists ? ProductsListReducer.ProductsLists : [],
    statusADD: AddToCartListReducer && AddToCartListReducer.statusADD ? AddToCartListReducer.statusADD : [],
    statusPR: ProductsListReducer && ProductsListReducer.statusPR ? ProductsListReducer.statusPR : '',
    messageWL: WishListReducer && WishListReducer.messageWL ? WishListReducer.messageWL : [],

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CardMap)