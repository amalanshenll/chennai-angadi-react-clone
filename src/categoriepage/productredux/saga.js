import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { PRODUCT_LIST } from '../../helpers/type'
import {
  productsRequest,
  productsSuccess,
  productsError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* categorieListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(productsRequest());
    const data = yield call(() => API.get(`${apiUrl.PRODUCT}${action.payload}`, params));
    yield put(productsSuccess(data));
  } catch (error) {
    yield put(productsError());
  }
}

function* fetchProductsRootSaga() {
  yield all([
    yield takeEvery(PRODUCT_LIST.GET_LIST, categorieListAsync)
  ]);
}

const fetchProductsSaga = [
  fork(fetchProductsRootSaga),
];

export default fetchProductsSaga;
