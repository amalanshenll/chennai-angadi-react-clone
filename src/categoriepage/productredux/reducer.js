import { PRODUCT_LIST } from '../../helpers/type'

function ProductsListReducer(state = {}, action) {
  switch (action.type) {
    case PRODUCT_LIST.REQUEST:
      return Object.assign({}, state, {
        loadingPR: true
      });
    case PRODUCT_LIST.SUCCESSS:
      return Object.assign({}, state, {
        loadingPR: false,
        ProductsLists: action.ProductsList,
        statusPR: action.statusPR,
        messagePR: action.messagePR,
        namePR: action.namePR,
        count: action.count
      });
    case PRODUCT_LIST.ERROR:
      return Object.assign({}, state, {
        loadingPR: false,
        error: true,
        ProductsLists: [],
        statusPR: action.statusPR,
        messagePR: action.messagePR,
        namePR: action.namePR,
      });
    case PRODUCT_LIST.CLEAR:
      return Object.assign({}, state, {
        loadingPR: false,
        error: true,
        ProductsLists: [],
        statusPR: '',
        messagePR: '',
        namePR:''
      });
    default:
      return state;
  }
}

export default ProductsListReducer;
