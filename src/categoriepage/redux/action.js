import { CATEGORIES_LIST } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function categorieList(data) {
  return {
    type: CATEGORIES_LIST.GET_LIST,
    payload: data
  }
}

export function categorieRequest() {
  return {
    type: CATEGORIES_LIST.REQUEST
  }
}

export function categorieSuccess(response) {
  const { data, status } = response
  return {
    type: CATEGORIES_LIST.SUCCESSS,
    statusCA: status,
    messageCA: response.message,
    categorieList: response
  }
}

export function categorieError(response) {
  return {
    type: CATEGORIES_LIST.ERROR,
    statusCA: ERROR.MSG,
    messageCA: response && response.message ? response.message : 'Something went wrong!'
  }
}