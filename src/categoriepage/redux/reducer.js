import { CATEGORIES_LIST } from '../../helpers/type'

function fetcCategorigeListReducer(state = {}, action) {
  switch (action.type) {
    case CATEGORIES_LIST.REQUEST:
      return Object.assign({}, state, {
        loadingCA: true
      });
    case CATEGORIES_LIST.SUCCESSS:
      return Object.assign({}, state, {
        loadingCA: false,
        billionairecategorieList: action.categorieList,
        statusCA: action.statusCA,
        messageCA: action.messageCA,
        count: action.count
      });
    case CATEGORIES_LIST.ERROR:
      return Object.assign({}, state, {
        loadingCA: false,
        error: true,
        billionairecategorieList: [],
        statusCA: action.statusCA,
        messageCA: action.messageCA
      });
    case CATEGORIES_LIST.CLEAR:
      return Object.assign({}, state, {
        loadingCA: false,
        error: true,
        billionairecategorieList: [],
        statusCA: '',
        messageCA: ''
      });
    default:
      return state;
  }
}

export default fetcCategorigeListReducer;
