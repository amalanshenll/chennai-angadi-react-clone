import { EDIT_PROFILE } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function EditProfileList(data) {
  return {
    type: EDIT_PROFILE.GET_LIST,
    payload: data
  }
}

export function AddressProfileRequest() {
  return {
    type: EDIT_PROFILE.REQUEST
  }
}

export function AddressProfileSuccess(response) {
  const { data, status } = response
  return {
    type: EDIT_PROFILE.SUCCESSS,
    statusAP: status,
    messageAP: response.message,
    AddressProfileList: response
  }
}

export function AddressProfileError(response) {
  return {
    type: EDIT_PROFILE.ERROR,
    statusAP: ERROR.MSG,
    messageAP: response && response.message ? response.message : 'Something went wrong!'
  }
}