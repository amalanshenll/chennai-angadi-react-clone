import { ADDRESS_PROFILE } from '../../helpers/type'

function AddressProfileReducer(state = {}, action) {
  switch (action.type) {
    case ADDRESS_PROFILE.REQUEST:
      return Object.assign({}, state, {
        loadingAP: true
      });
    case ADDRESS_PROFILE.SUCCESSS:
      return Object.assign({}, state, {
        loadingAP: false,
        ProfileLists: action.AddressProfileList,
        statusAP: action.statusAP,
        messageAP: action.messageAP,
        count: action.count
      });
    case ADDRESS_PROFILE.ERROR:
      return Object.assign({}, state, {
        loadingAP: false,
        error: true,
        ProfileLists: [],
        statusAP: action.statusAP,
        messageAP: action.messageAP
      });
    case ADDRESS_PROFILE.CLEAR:
      return Object.assign({}, state, {
        loadingAP: false,
        error: true,
        ProfileLists: [],
        statusAP: '',
        messageAP: ''
      });
    default:
      return state;
  }
}

export default AddressProfileReducer;
