import { ADDRESS_PROFILE } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function AddressProfileList(data) {
  return {
    type: ADDRESS_PROFILE.GET_LIST,
    payload: data
  }
}

export function AddressProfileRequest() {
  return {
    type: ADDRESS_PROFILE.REQUEST
  }
}

export function AddressProfileSuccess(response) {
  const { data, status } = response
  return {
    type: ADDRESS_PROFILE.SUCCESSS,
    statusAP: status,
    messageAP: response.message,
    AddressProfileList: response
  }
}

export function AddressProfileError(response) {
  return {
    type: ADDRESS_PROFILE.ERROR,
    statusAP: ERROR.MSG,
    messageAP: response && response.message ? response.message : 'Something went wrong!'
  }
}