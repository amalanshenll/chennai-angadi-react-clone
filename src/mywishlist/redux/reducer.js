import { WISH_LIST } from '../../helpers/type'

function WishListReducer(state = {}, action) {
  switch (action.type) {
    case WISH_LIST.REQUEST:
      return Object.assign({}, state, {
        loadingWL: true
      });
    case WISH_LIST.SUCCESSS:
      return Object.assign({}, state, {
        loadingWL: false,
        WishLists: action.WishList,
        statusWL: action.statusWL,
        messageWL: action.messageWL,
        namePR: action.namePR,
        count: action.count
      });
    case WISH_LIST.ERROR:
      return Object.assign({}, state, {
        loadingWL: false,
        error: true,
        WishLists: [],
        statusWL: action.statusWL,
        messageWL: action.messageWL,
        namePR: action.namePR,
      });
    case WISH_LIST.CLEAR:
      return Object.assign({}, state, {
        loadingWL: false,
        error: true,
        WishLists: [],
        statusWL: '',
        messageWL: '',
        namePR:''
      });
    default:
      return state;
  }
}

export default WishListReducer;
