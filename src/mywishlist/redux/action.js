import { WISH_LIST } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export  function WishList(data) {
  return {
    type: WISH_LIST.GET_LIST,
    payload: data
  }
}

export function WishListRequest() {
  return {
    type: WISH_LIST.REQUEST
  }
}

export function WishListSuccess(response) {
  const { data, status } = response
  return {
    type: WISH_LIST.SUCCESSS,
    statusWL: status,
    messageWL: response.message,
    namePR: response.namePR,
    WishList: data
  }
}

export function WishListError(response) {
  return {
    type: WISH_LIST.ERROR,
    statusWL: ERROR.MSG,
    messageWL: response && response.message ? response.message : 'Something went wrong!'
  }
}