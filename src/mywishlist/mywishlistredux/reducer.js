import { MY_WISH_LIST } from '../../helpers/type'

function MyWishListReducer(state = {}, action) {
  switch (action.type) {
    case MY_WISH_LIST.REQUEST:
      return Object.assign({}, state, {
        loadingMWL: true
      });
    case MY_WISH_LIST.SUCCESSS:
      return Object.assign({}, state, {
        loadingMWL: false,
        MyWishLists: action.MyWishList,
        statusMWL: action.statusMWL,
        messageWL: action.messageWL,
        count: action.count
      });
    case MY_WISH_LIST.ERROR:
      return Object.assign({}, state, {
        loadingMWL: false,
        error: true,
        MyWishLists: [],
        statusMWL: action.statusMWL,
        messageWL: action.messageWL,
      });
    case MY_WISH_LIST.CLEAR:
      return Object.assign({}, state, {
        loadingMWL: false,
        error: true,
        MyWishLists: [],
        statusMWL: '',
        messageWL: ''
      });
    default:
      return state;
  }
}

export default MyWishListReducer;
