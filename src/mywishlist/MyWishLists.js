import React, { useState } from 'react'
import '../landinpage/index.css'
import Termsheaders from '../header/CommonHeader'
import termunderline_img from '../assets/images/termunderline_img.png'
import Wishlistcardmap from './WishListsCard'
import Footer from '../components/Footer';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';

function Wishlist() {
  return (
    <Container className="themed-container" fluid={true}>
      <div>
        <Termsheaders />
      </div>
      <div className="terms-heading mb-4">
        <h1 className="terms-head">My Wish List</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <div className="wishlistpage mb-5">
        <Container>
          <Row className="categories-card">
            <Col xs={12} md={3} mx-auto>
              <Wishlistcardmap />
            </Col>
          </Row>
        </Container>
      </div>
      < Footer />
    </Container>
  )
}
export default Wishlist