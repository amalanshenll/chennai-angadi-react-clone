import { PRODUCT_DETAIL } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function ProductDetailList(data) {
  return {
    type: PRODUCT_DETAIL.GET_LIST,
    payload: data
  }
}

export function ProductDetailRequest() {
  return {
    type: PRODUCT_DETAIL.REQUEST
  }
}

export function ProductDetailSuccess(response) {
  const { data, status } = response
  return {
    type: PRODUCT_DETAIL.SUCCESSS,
    statusPD: status,
    messagePD: response.message,
    namePD: response.namePR,
    ProductDetailList: data,
    id:data.id
  }
}

export function ProductDetailError(response) {
  return {
    type: PRODUCT_DETAIL.ERROR,
    statusPR: ERROR.MSG,
    messagePR: response && response.message ? response.message : 'Something went wrong!'
  }
}