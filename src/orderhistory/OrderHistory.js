import React, { useState } from 'react'
import '../landinpage/index.css'
import Termsheaders from '../header/CommonHeader'
import termunderline_img from '../assets/images/termunderline_img.png'
import Footer from '../components/Footer';
import checkout_img from '../assets/images/checkout_img.png'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardText
} from 'reactstrap';

function Orderhistorypage() {
  return (
    <Container className="themed-container1" fluid={true}>
      <div>
        <Termsheaders />
      </div>
      <div className="terms-heading">
        <h1 className="terms-head">Order History</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <Col xs={12} md={12} mx-auto className=" order-history-cards mb-5">
        <div>
          <Card className="order-history-card mb-3">
            <CardBody>
              <Row>
                <Col xs={12} md={2} mx-auto className="mb-5">
                  <CardImg className="checkout-img" src={checkout_img} alt="Card image cap" />
                </Col>
                <Col xs={12} md={4} mx-auto className="mb-5">
                  <div>
                    <p className="order-history-no mb-0">#20323652</p>
                    <p className="order-history-organic mb-0">Organic Strawberry</p>
                    <p className="order-history-kg mb-0">0.5kg</p>
                    <span class="categires-webrupee">&#x20B9;</span>
                    <span className='amount' >200</span>
                  </div>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-date mb-0">Order Date</h1>
                  <p className="order-history-date1">02-04-2021</p>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-deliver mb-0">To Deliver On</h1>
                  <p className="order-history-date1">22-04-2021</p>
                  <div>
                    <span>
                      <Button className="order-history-trackbtn">
                        Track
                      </Button>
                    </span>
                    <span className='order-history-cancel' >Cancel</span>
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
        <div>
          <Card className="order-history-card1 mb-3">
            <CardBody>
              <Row>
                <Col xs={12} md={2} mx-auto className="mb-5">
                  <CardImg className="checkout-img" src={checkout_img} alt="Card image cap" />
                </Col>
                <Col xs={12} md={4} mx-auto className="mb-5">
                  <div>
                    <p className="order-history-no mb-0">#20323652</p>
                    <p className="order-history-organic mb-0">Organic Strawberry</p>
                    <p className="order-history-kg mb-0">0.5kg</p>
                    <span class="categires-webrupee">&#x20B9;</span>
                    <span className='amount' >200</span>
                  </div>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-date mb-0">Order Date</h1>
                  <p className="order-history-date1">02-04-2021</p>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-deliver mb-0">Status</h1>
                  <p className="order-history-deliverd">Delivered</p>
                  <div>
                    <span>
                      <Button className="order-history-receiptkbtn">
                        Receipt
                      </Button>
                    </span>
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
        <div>
          <Card className="order-history-card1">
            <CardBody>
              <Row>
                <Col xs={12} md={2} mx-auto className="mb-5">
                  <CardImg className="checkout-img" src={checkout_img} alt="Card image cap" />
                </Col>
                <Col xs={12} md={4} mx-auto className="mb-5">
                  <div>
                    <p className="order-history-no mb-0">#20323652</p>
                    <p className="order-history-organic mb-0">Organic Strawberry</p>
                    <p className="order-history-kg mb-0">0.5kg</p>
                    <span class="categires-webrupee">&#x20B9;</span>
                    <span className='amount' >200</span>
                  </div>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-date mb-0">Order Date</h1>
                  <p className="order-history-date1">02-04-2021</p>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-deliver mb-0">Status</h1>
                  <p className="order-history-cancelled">Cancelled</p>
                  <div>
                    <span>
                      <Button className="order-history-receiptkbtn">
                        Receipt
                      </Button>
                    </span>
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
      </Col>
      <div className="order-history-fotter">
        < Footer />
      </div>
    </Container>
  )
}
export default Orderhistorypage