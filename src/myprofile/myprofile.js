import React, { useState, useEffect } from 'react';
import {
  Card, CardImg, CardBody, Button, Input, Alert
} from 'reactstrap';
import { Row, Col } from 'reactstrap';
import "./myprofile.scss";
import Vector from './images/Vector.png';
import Ellipse from './images/Ellipse.png';
import termunderline_img from '../assets/images/termunderline_img.png'
import Footer from '../components/Footer';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Termsheaders from '../header/CommonHeader'
import { REGISTERATION_FORM_MSGS, LOGIN_MGS } from '../helpers/message'
import { ProfileList } from '../myprofile/redux/action'
import { connect } from 'react-redux'
import ImageDropzone from '../fileUpload/ImageDropzone'
import { DROPZONE_IMAGE_NAME_TYPES } from '../helpers/type'

import {
  LOCAL_STORAGE,
  REGEX,
  FILE_FORMAT_TYPE,
  STATUS_RESPONSE,
  VERIFY_MESSAGE_RESPONSE,
  JWT_EXPIRED,
} from '../helpers/const'
import { Form, FormGroup, Label, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
}
);
function Example(props) {

  const {
    dispatch,
    statusCA
  } = props

  const classes = useStyles();
  const [errors, setErrors] = useState({})
  const [defaultImages, setDefaultImages] = useState([])
  const [toggle, setToggle] = useState(false)
  const [toggleerror, setToggleerror] = useState(false)

  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    gender: 'MALE',
    mobile_number: '',
    email: '',
  })

  const validate = () => {
    let valid = true
    const errors = {}
    if (!formData.mobile_number) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.USER_MOBILENUMBER_REQUIRED
      valid = false
    } else if (!REGEX.MOBILE.test(formData.mobile_number)) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.MOBILE_NUMBER_INVALID
      valid = false
    }
    if (!formData.email) {
      errors.email = REGISTERATION_FORM_MSGS.USER_EMAIL_REQUIRED
      valid = false
    } else if (!REGEX.EMAIL.test(formData.email)) {
      errors.email = REGISTERATION_FORM_MSGS.EMAIL_INVALID
      valid = false
    }
    if (!formData.first_name) {
      errors.first_name = REGISTERATION_FORM_MSGS.FIRST_NAME_REQUIRED
      valid = false
    } else if (!REGEX.NAME.test(formData.first_name)) {
      errors.first_name = REGISTERATION_FORM_MSGS.FIRST_NAME_INVALID
      valid = false
    }
    if (!formData.last_name) {
      errors.last_name = REGISTERATION_FORM_MSGS.LAST_NAME_REQUIRED
      valid = false
    } else if (!REGEX.NAME.test(formData.last_name)) {
      errors.last_name = REGISTERATION_FORM_MSGS.LAST_NAME_INVALID
      valid = false
    }
    setErrors(errors)
    return valid
  }

  function loginChange(e) {
    const { name, value } = e.target;
    setFormData(formData => ({ ...formData, [name]: value }));
  }

  function loginSubmit() {
    if (validate()) {
      dispatch(ProfileList(formData))
    }
  }

  useEffect(() => {
    if (statusCA === STATUS_RESPONSE.SUCCESS_MSG) {
      setToggle(true)
      props.history.push('/addressprofile/$')
      const timer = setTimeout(() => {
        setToggle(false);
      }, 3000);
    } else if (statusCA === STATUS_RESPONSE.ERROR) {
      setToggleerror(true)
      const timer = setTimeout(() => {
        setToggleerror(false);
      }, 3000);
    }
  }, [statusCA])

  return (
    <div className="container-fluid prof">
      <Termsheaders />
      <div className="terms-heading">
        <h1 className="terms-head">My Profile</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <div className="myprofile">
        <Alert color="danger" isOpen={toggleerror} fade={false}>
          My Profile Have Error
        </Alert>
        <Alert color="success" isOpen={toggle} fade={false}>
          Address added successfully
        </Alert>
        <div className="img-round ">
        <ImageDropzone 
            defaultImages={defaultImages} type={DROPZONE_IMAGE_NAME_TYPES.PROFILE}
            multiple={false} thumbnail={false} width={100} height={100} maxFileSize='1'
            name={`${DROPZONE_IMAGE_NAME_TYPES.PROFILE.toLowerCase()}`}
            flag={DROPZONE_IMAGE_NAME_TYPES.PROFILE} formatType={FILE_FORMAT_TYPE}
          />
          <CardImg className="img-vec" src={Vector} />
        </div>
        <div className="userprof">
          <Card>
            <div className="cam-img">
              <i class="fa fa-camera" aria-hidden="true"></i>
            </div>
            <CardBody className="card address-bg">
              <div className="add-bg">
                <Row>
                  <Col xs="5">
                    <Input
                      className="textfield"
                      type="text" size="sm"
                      placeholder="First name"
                      name="first_name"
                      value={formData.first_name}
                      onChange={loginChange}
                    />
                    {
                      errors.first_name && (<div className='text-danger'> {errors.first_name} </div>)
                    }
                  </Col>
                  <Col xs="7" className="mb-2">
                    <Input
                      className="textfield-lname"
                      type="text" size="sm"
                      placeholder="Last name"
                      name="last_name"
                      value={formData.last_name}
                      onChange={loginChange}
                    />
                    {
                      errors.last_name && (<div className='text-danger'> {errors.last_name} </div>)
                    }
                  </Col>
                  <Col sm="5" className="mb-2">
                    <span className="gender"> Gender
                      <span className="profile-radio-btn">
                        <FormGroup check >
                          <Label check>
                            <Input type="radio"
                              name="gender"
                              id="male"
                              value={formData.gender}
                              onChange={loginChange}
                            />{' '}
                            male
                          </Label>
                        </FormGroup>
                        {
                          errors.male && (<div className='text-danger'> {errors.male} </div>)
                        }
                        <FormGroup check className="radio-btn-male">
                          <Label check>
                            <Input type="radio"
                              name="gender"
                              id="female"
                              value={formData.gender}
                              onChange={loginChange}
                            />{' '}
                            female
                          </Label>
                        </FormGroup>
                        {
                          errors.female && (<div className='text-danger'> {errors.female} </div>)
                        }
                      </span>
                    </span>
                  </Col>
                  <Col xs="7">
                  </Col>
                  <Col xs="5">
                    <Input
                      className="textfield-mob "
                      type="number"
                      size="sm"
                      placeholder="Mobile Number"
                      name="mobile_number"
                      value={formData.mobile_number}
                      onChange={loginChange}
                    />
                    {
                      errors.mobile_number && (<div className='text-danger'> {errors.mobile_number} </div>)
                    }
                  </Col>
                  <Col xs="7">
                    <Input
                      className="textfield-email"
                      type="email"
                      size="sm"
                      placeholder="Email Id"
                      name="email"
                      value={formData.email}
                      onChange={loginChange}
                    />
                    {
                      errors.email && (<div className='text-danger'> {errors.email} </div>)
                    }
                  </Col>
                  <Col xs="5">
                  </Col>
                  <Col xs="7">
                      <Button
                        className="button"
                        color="success"
                        onClick={loginSubmit}
                      >
                        Save
                      </Button>{' '}
                  </Col>
                </Row>
              </div>
            </CardBody>
          </Card>
        </div>
      </div>
      <Footer />
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { fetcProfileReducer } = state
  return {
    statusCA: fetcProfileReducer ? fetcProfileReducer.statusCA : [],
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Example)