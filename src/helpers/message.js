export const REGISTERATION_FORM_MSGS = {
    USER_EMAIL_REQUIRED: 'Email is required',
    EMAIL_INVALID:' Email is invalid',
    USER_MOBILENUMBER_REQUIRED: ' Mobile Number is required.',
    MOBILE_NUMBER_INVALID:'Invalid mobile Number',
    FIRST_NAME_REQUIRED:'First Name is required',
    FIRST_NAME_INVALID:'First Name is invalid',
    LAST_NAME_REQUIRED:'Last Name is required',
    LAST_NAME_INVALID:'Last Name is invalid'
  }
  
  export const LOGIN_MGS = {
    MOBILE_REQUIRED: 'Mobile Number is required.',
    MOBILE_INVALID: 'Invalid mobile Number.',
    MOBILE_NUMBER_NOT_REGISTERED: 'Please enter registered  mobile Number.',
    MOBILE_NUMBER_SUBSCRIPTION_ERR: 'Please check your payment status.',
    OTP_REQUIRED: 'OTP Number is required.',
    OTP_INVALID: 'Invalid OTP Number.',
    TYPE_REQUIRED: 'Type Name is required.',
  }

  export const ADRESS_FIELD_MGS = {
    ADRESS_REQUIRED: 'Address field is required.'
  }