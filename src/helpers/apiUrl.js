const ENV = process.env.REACT_APP_API_URL;

export const apiUrl = {
  USER: `${ENV}/user/signUp`,
  OTP: `${ENV}/otp/verifyOtp`,
  LOGIN: `${ENV}/auth/authentication`,
  CATEGORY: `${ENV}/category`,
  PRODUCT: `${ENV}/product?category=`,
  WISHLIST: `${ENV}/wishlist`,
  PRODUCT_DETAIL: `${ENV}/product/`,
  OREDER: `${ENV}/orders`,
  PROFILE: `${ENV}/user`,
  ADDRESSPROFILE: `${ENV}/address`,
  USERPROFILE: `${ENV}/user`,
  FILEUPLOAD: `${ENV}/fileuploads`,
  CHECKOUT: `${ENV}/orders/checkout`,
}