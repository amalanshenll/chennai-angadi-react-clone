import { UPLOAD_IMAGE } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function uploadImage(data) {
  return {
    type: UPLOAD_IMAGE.GET_LIST,
    payload: data
  }
}

export function uploadRequest() {
  return {
    type: UPLOAD_IMAGE.REQUEST
  }
}

export function uploadSuccess(response) {
  const { data, status } = response
  return {
    type: UPLOAD_IMAGE.SUCCESSS,
    statusIMG: status,
    messageIMG: response.message,
    uploadImage: response
  }
}

export function uploadError(response) {
  return {
    type: UPLOAD_IMAGE.ERROR,
    statusIMG: ERROR.MSG,
    messageIMG: response && response.message ? response.message : 'Something went wrong!'
  }
}