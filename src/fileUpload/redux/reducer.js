import { UPLOAD_IMAGE } from '../../helpers/type'

function dropzoneReducer(state = {}, action) {
  switch (action.type) {
    case UPLOAD_IMAGE.REQUEST:
      return Object.assign({}, state, {
        loadingIMG: true,
        image: [],
        imageName: ''
      });
    case UPLOAD_IMAGE.SUCCESSS:
      return Object.assign({}, state, {
        loadingIMG: false,
        uploadImages: action.uploadImage,
        image: action.image,
        statusIMG: action.statusIMG,
        messageIMG: action.messageIMG,
        count: action.count
      });
    case UPLOAD_IMAGE.ERROR:
      return Object.assign({}, state, {
        loadingIMG: false,
        image: [],
        error: true,
        uploadImages: [],
        statusIMG: action.statusIMG,
        messageIMG: action.messageIMG
      });
    case UPLOAD_IMAGE.CLEAR:
      return Object.assign({}, state, {
        loadingIMG: false,
        image: action.image,
        error: true,
        uploadImages: [],
        statusIMG: '',
        messageIMG: ''
      });
    default:
      return state;
  }
}

export default dropzoneReducer;
