import React, { useState, useEffect, useCallback } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'
import { useDropzone } from 'react-dropzone'
import { uploadImage, removeImage, removeImageWithThumbnail } from './redux/action'
import { EMPTY_DROPZONE, ERROR_MSG } from '../helpers/const'
import defaultImage from '../assets/images/default.png'
import { DELETE_THUMBNAIL_IMAGE } from '../helpers/type'

function ImageDropzone (props) {
  const { defaultImages, multiple, thumbnail, width, height, type, dispatch, name, flag, maxFileSize, formatType } = props
  const imagesArray = defaultImages && defaultImages.length > 0 ? _.filter(defaultImages, (i) => { return i.key }) : []
  const [files, setFiles] = useState(imagesArray)
  const [status, setStatus] = useState("Drag 'n' drop some files here, or click to select files")
  const [error, setError] = useState(false)
  const isJsonFormat = _.includes(formatType, 'application/json')

  const onDrop = useCallback((acceptedFiles) => {
    setError(false)
    setStatus('Uploading...')
    let isValidFileFormat = false
    const formData = new FormData()
    formData.append('type', type)
    if (thumbnail) {
      formData.append('width', width)
      formData.append('height', height)
    }
    acceptedFiles.forEach((item) => {
      const fileType = item && item.type
      const uploadedFileSize = item && item.size
      isValidFileFormat = _.includes(formatType, fileType)
      const fileSize = Number(maxFileSize) * 1024 * 1024
      if (isValidFileFormat) {
        if (uploadedFileSize < fileSize) {
          formData.append('file_to_upload', item)
        } else {
          setError(true)
          setStatus(`File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`)
        }
      } else {
        setError(true)
        setStatus(`${isJsonFormat ? ERROR_MSG.JSON_INVALID : ERROR_MSG.IMAGE_INVALID}`)
      }
    })

    if (isValidFileFormat) {
      if (thumbnail) {
        dispatch(uploadImage(formData,name, flag))
      } else {
        dispatch(uploadImage(formData, name, flag))
      }
    }
  }, [])

  const { getRootProps, getInputProps } = useDropzone({ onDrop })

  function removeDropzoneImage (key, flag) {
    setFiles([])
    setStatus(EMPTY_DROPZONE.MSG)
    dispatch({type: DELETE_THUMBNAIL_IMAGE.RESPONSE_SUCCESS, flag})
  }

  const previewImages = files.map((image, i) => (
    <div className={`${image.key} carousel-item ${i === 0 ? 'active' : ''}`} key={image.key}>
      <img
        className='preview-image' src={image.location} alt={image.originalname || ''} onError={(e) => {
          e.target.src = defaultImage
        }}
      />
      <div className='carousel-caption p-0' style={{ bottom: '35px', position: 'initial' }}>
        <p className='btn btn-danger btn-sm' style={{ margin: '5px 0px' }} onClick={() => { removeDropzoneImage(image.key, flag) }}>
          <i className='mdi mdi-delete' /> Remove Image
        </p>
      </div>
    </div>
  ))

  useEffect(() => {
    const imagesArray = defaultImages && defaultImages.length > 0 ? _.filter(defaultImages, (i) => { return i.key }) : []
    if (_.isEmpty(imagesArray)) {
      setStatus(`${isJsonFormat ? EMPTY_DROPZONE.FILE_MSG : EMPTY_DROPZONE.MSG}`)
    }
    setFiles([...imagesArray])
  }, [defaultImages])

  return (
    <div className='dropzone-container'>
      {
        files && files.length === 0
          ? (
            <div {...getRootProps({ className: `dropzone ${error && 'dropzone-error'}` })}>
              <input {...getInputProps({ multiple })} />
              <p className='mt-5 text-12'><i className='nav-icon i-Share-on-Cloud text-14' />{status}</p>
            </div>
            )
          : null
      }
      {
        files && files.length > 0
          ? (
            <div className='preview'>
              <div id='previewControl' className='carousel slide' data-ride='carousel'>
                <div className='carousel-inner  image-uploader-bg'>
                  {previewImages}
                </div>
                {
                  multiple
                    ? (
                      <a className='carousel-control-prev' href='#previewControl' role='button' data-slide='prev'>
                        <span className='carousel-control-prev-icon' aria-hidden='true' />
                        <span className='sr-only'>Previous</span>
                      </a>
                      )
                    : null
                }
                {
                  multiple
                    ? (
                      <a className='carousel-control-next' href='#previewControl' role='button' data-slide='next'>
                        <span className='carousel-control-next-icon' aria-hidden='true' />
                        <span className='sr-only'>Next</span>
                      </a>
                      )
                    : null
                }
              </div>
            </div>
            )
          : null
          }
    </div>
  )
}

const mapDispatchToProps = dispatch => ({ dispatch })

const mapStateToProps = function (state) {
  const { dropzoneReducer } = state
  return {
    dropzoneImage: dropzoneReducer && dropzoneReducer.image ? dropzoneReducer.image : [],
    dropzoneImageThumbNail: dropzoneReducer && dropzoneReducer.imageThumbNail ? dropzoneReducer.imageThumbNail : [],
    dropzoneStatus: dropzoneReducer && dropzoneReducer.status ? dropzoneReducer.status : ''
  }
}

ImageDropzone.propTypes = {
  dropzoneImage: PropTypes.array,
  dropzoneImageThumbNail: PropTypes.array,
  dropzoneStatus: PropTypes.string
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ImageDropzone)
