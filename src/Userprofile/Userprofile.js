import React, { useState, useEffect } from 'react'
import {
  Card, CardImg, CardText, CardBody,
} from 'reactstrap';
import "./Profile.scss";
import AddLocationIcon from '@material-ui/icons/AddLocation';
import termunderline_img from '../assets/images/termunderline_img.png'
import Vector from './images/Vector.png';
import Footer from '../components/Footer';
import Termsheaders from '../header/CommonHeader';
import { green, red } from '@material-ui/core/colors';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { UserProfileList } from './redux/action'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { DeleteProfileList } from './deleteredux /action'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },

  largeIcon: {
    marginBottom: 5,
    fontSize: '18px',
    color: "#83A43E",
  }
}));
const Example = (props) => {

  const {
    dispatch,
    UserProfileLists,
    match
  } = props

  const { data } = UserProfileLists && UserProfileLists
  const classes = useStyles();
  const [selectedItem, setSelectedItem] = useState({})
  const matchId = match && match.params && match.params.id

  useEffect(() => {
    dispatch(UserProfileList())
  }, [])

  function onDeleteItem (id) {
    dispatch(DeleteProfileList(id))
  }


  return (
    <div className="container-fluid prof">
      < Termsheaders />
      <div className="terms-heading">
        <h1 className="terms-head">User Profile</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      {
        data && data.map((item, i) => {
          return (
            <div key={item._id} onClick={() => setSelectedItem(item)}>
                <div className="img-circle ">
                  <CardImg className="img-vector" src={Vector} />
                </div>
                <div className="card title-user">
                  <Card>
                    <i class="fa fa-pencil-square-o edit" aria-hidden="true"></i>
                    <CardBody className="card-name">
                      <h6>
                        {
                          item.first_name
                        }
                      </h6>
                      <h6 className="case-bg">
                        {
                          item.mobile_number
                        }
                      </h6>
                      <h6 className="case-bg">
                        {
                          item.email
                        }
                      </h6>
                    </CardBody>
                  </Card>
                </div>
                <div className="user">
                  <Card>
                    <CardBody className="card-address">
                      <h6>Manage Address </h6>
                      <div className="add">
                        <AddLocationIcon className={classes.largeIcon} />
                        <span className="new-address">  Add New Address </span>
                      </div>
                      <div className="address">
                        <div className="add-text">
                          <Link
                            to={`/addressprofile/${item._id}`} className='mr-2'
                          >
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                          </Link>
                          <h6>
                            {
                              item.first_name
                            }
                          </h6>
                          <Link  to='' className='mr-2' 
                          onClick={() => onDeleteItem(item._id)}
                          >
                          <i class="fa fa-trash" aria-hidden="true" ></i>
                          </Link>
                          <h6 className="case-bg">
                            {
                              item.mobile_number
                            }
                          </h6>
                          <h6 className="case-bg">Plot No.1069 Munusamy Salai</h6>
                          <span className="del">Delivery Address<i class="fa fa-heart-o" aria-hidden="true"></i>
                          </span>
                          <h6 className="case-bg">k.k.Nagar Chennai-600078</h6>
                        </div>
                      </div>
                      <div className="address">
                        <div className="add-text">
                          <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                          <h6>
                            {
                              item.first_name
                            }                        </h6>
                          <i class="fa fa-trash" aria-hidden="true"></i>
                          <h6 className="case-bg">
                            {
                              item.mobile_number
                            }
                          </h6>
                          <h6 className="case-bg">Plot No.1069 Munusamy Salai</h6>
                          <h6 className="case-bg">k.k.Nagar Chennai-600078</h6>
                          <span className="del-address">Delivery Address<i class="fa fa-heart" aria-hidden="true"></i>
                          </span>
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </div>
            </div>
          )
        })
      }
      <Footer />
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { UserProfileReducer } = state
  return {
    UserProfileLists: UserProfileReducer && UserProfileReducer.UserProfileLists ? UserProfileReducer.UserProfileLists : [],
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Example)